package com.sm.quiz.api.input;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 */
@Setter
@Getter
public class BalancedInput {

    private String content;
}
