package com.sm.quiz.service.controller;

import com.sm.quiz.api.input.BalancedInput;
import com.sm.quiz.service.command.BalancedProcessCmd;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Santiago Mamani
 */
@RestController
@RequestMapping(value = "/balanced")
@RequestScope
public class BalancedController {

    @Autowired
    private BalancedProcessCmd balancedProcessCmd;

    @ApiOperation(
            value = "Balanced functionality"
    )
    @PostMapping
    public Boolean isBalanced(@RequestBody BalancedInput input) {
        balancedProcessCmd.setInput(input);
        balancedProcessCmd.execute();

        return balancedProcessCmd.getIsBalanced();
    }
}
