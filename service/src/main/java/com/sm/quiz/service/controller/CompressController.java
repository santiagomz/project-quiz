package com.sm.quiz.service.controller;

import com.sm.quiz.service.command.CompresStringCmd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Santiago Mamani
 */
@RestController
@RequestMapping(value = "/compress")
@RequestScope
public class CompressController {

    @Autowired
    private CompresStringCmd commpesStringCmd;

    @GetMapping
    public String compress(@RequestParam String input) {
        commpesStringCmd.setInput(input);
        commpesStringCmd.execute();

        return commpesStringCmd.getResult();
    }
}
