package com.sm.quiz.service.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.sm.quiz.api.input.BalancedInput;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author Santiago Mamani
 * Project-in-git: https://gitlab.com/santiagomz/project-quiz
 */
@SynchronousExecution
public class BalancedProcessCmd implements BusinessLogicCommand {

    @Setter
    private BalancedInput input;

    @Getter
    private Boolean isBalanced;

    @Override
    public void execute() {
        Deque<Character> deque = new ArrayDeque<>();
        String content = input.getContent();
        isBalanced = Boolean.TRUE;

        int i = 0;
        while (i < content.length() && Boolean.TRUE.equals(isBalanced)) {
            char charAt = content.charAt(i);

            if (!isPresentCharacterOpen(charAt, deque) && isCharacterClose(charAt)) {
                isBalanced = isBalancedChar(charAt, deque);
            }

            i++;
        }
        isBalanced = deque.isEmpty();
    }

    private boolean isCharacterClose(char charAt) {
        return charAt == ')' || charAt == '}' || charAt == ']';
    }

    private boolean isBalancedChar(char charAt, Deque<Character> deque) {
        return !deque.isEmpty() && deque.pop() == charAt;
    }

    private boolean isPresentCharacterOpen(char charAt, Deque<Character> deque) {
        if (charAt == '(') {
            deque.push(')');
            return true;
        }
        if (charAt == '{') {
            deque.push('}');
            return true;
        }

        if (charAt == '[') {
            deque.push(']');
            return true;
        }

        return false;
    }
}
