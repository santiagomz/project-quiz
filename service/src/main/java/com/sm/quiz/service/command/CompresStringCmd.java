package com.sm.quiz.service.command;

import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

/**
 * @author Santiago Mamani
 */
@SynchronousExecution
public class CompresStringCmd implements BusinessLogicCommand {

    @Setter
    private String input;

    @Getter
    private String result;

    @Override
    public void execute() {
        result = "";

        if (StringUtils.isEmpty(input)) {
            return;
        }

        char letterBefore = input.charAt(0);
        int count = 0;
        for (int i = 0; i < input.length(); i++) {
            char letterCurrent = input.charAt(i);
            if (letterCurrent == letterBefore) {
                count++;
            } else {
                buildResult(count, letterBefore);
                count = 1;
                letterBefore = input.charAt(i);
            }
        }
        buildResult(count, letterBefore);
    }

    private void buildResult(int count, char letterBefore) {
        if (count <= 1) {
            result = result + letterBefore;
        } else {
            result = result + letterBefore + count;
        }
    }
}
